import {NgModule} from '@angular/core';
import { RouterModule, Routes} from '@angular/router';
import { LoginPage } from './login/login.page';
import { PokedexPage } from './pokedex/pokedex.page';
import { TrainerPage } from './trainer/trainer.page';
import { AuthGuard } from "./services/auth.guard";

const routes: Routes = [{
    path: "",
    component: LoginPage,
    pathMatch: 'full'
}, 
{
    path: "pokedex",
    component: PokedexPage,
    canActivate: [AuthGuard]
}, 
{
    path: "trainer",
    component: TrainerPage,
    canActivate: [AuthGuard]
}
]

@NgModule({
    imports: [RouterModule.forRoot(routes)],
    exports: [RouterModule],
})
export class AppRoutingModule {}