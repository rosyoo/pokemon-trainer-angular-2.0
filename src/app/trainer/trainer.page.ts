import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { PokemonService } from '../services/pokemon.service';
import { UserService } from '../services/user.service';
import { User } from '../models/user.model';

@Component({
  selector: 'app-trainer',
  templateUrl: './trainer.page.html',
  styleUrls: ['./trainer.page.css']
})
export class TrainerPage implements OnInit {
  private _user : User | undefined;
  private _userPokemons : any = [];

  //Check if user in storage else return console.log message and undefined
  checkIfUser(){
    let storedUser: string | null = localStorage.getItem("user")
    if(storedUser !== null){
      return JSON.parse(storedUser) as User
    }else{
      console.log("User is either null or undefined")
      return undefined;
    }
  }
  
  getUsername(){ //get username of stored user
    if(this._user && this._user.username !== undefined){
      return this._user.username;
    }else{
      console.log("No username provided")
      return "";
    }
  }

  getUserPokemonList(){ //get pokemonlist of stored user
    if(this._user && this._user.pokemon !== undefined){
      return this._user.pokemon;
    }else{
      console.log("No user pokemonList provided")
      return [];
    }
  }

  onReleasePokemon(pokemon: string){//Delete the pokemon from user localStorage
    let index : number | undefined = this._user?.pokemon.indexOf(pokemon)
    if(this._user?.pokemon !== undefined && index !== undefined){
      this._user.pokemon.splice(index, 1);
      localStorage.setItem("user", JSON.stringify(this._user))
    }
  }

  onClickLogout(){
    this.userService.logout();
  }

  onClickPokedex(){
    this.router.navigate(["pokedex"])
  }

  constructor(private router: Router, private pokemonService: PokemonService, private userService: UserService) { }

  ngOnInit(): void {
    let storedPokemons = localStorage.getItem('pokemons');
    this._user = this.checkIfUser();

    if(!this._user) { // if user is not logged in, navigate to login page
      this.router.navigate([""])
    }
    else if(storedPokemons !== null){ // get stored pokemons in storage
      this._userPokemons = JSON.parse(storedPokemons)
    }
    else{
      this.pokemonService.getAllPokemons()
      .subscribe({next : (response) => {
        this._userPokemons = response.results;
        sessionStorage.setItem("pokemons", JSON.stringify(response.results));
      },
      error: (error) => {
        console.log(error.message);
      }
    })
    }

  }

}
