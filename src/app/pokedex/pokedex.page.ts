import { Component, OnInit} from '@angular/core';
import { PokemonResponse} from "../models/pokemon.model"
import { PokemonService } from "../services/pokemon.service"
import { UserService } from '../services/user.service';
import { User } from "../models/user.model";
import { environment} from "src/environments/environment";

import { catchError} from 'rxjs';

import { HttpClient } from "@angular/common/http";
import { Router } from '@angular/router';

@Component({
  selector: 'app-pokedex',
  templateUrl: './pokedex.page.html',
  styleUrls: ['./pokedex.page.css']
})
export class PokedexPage implements OnInit {
  pokemons: PokemonResponse | any = [];
  storedUser : User = {id: 0, username: "", pokemon: []} //destructuring
  pokeImage = environment.imageUrl

  constructor(private pokemonService: PokemonService, private userService: UserService, private http: HttpClient, private router: Router) { }

  ngOnInit(): void {
    let storedUser = localStorage.getItem("user");
    let storedPokemons = sessionStorage.getItem("pokemons")
    
    //if the stored pokemons is empty, fetch all the pokemons from api
    if(storedPokemons == null){
      this.pokemonService.getAllPokemons().subscribe({
        next: (response) => {
          this.pokemons.results = response.results
          sessionStorage.setItem("pokemons", JSON.stringify(response.results));
        },
      }), catchError((error) => { throw error.error.error})
    } else{ //If not empty, parse and read info from storage
      this.pokemons.results = JSON.parse(storedPokemons);
    }

    //If there are no stored user, navigate to login page
    if(!storedUser){
      this.router.navigate(["login"])  
    }else{ //else, parse and get storage info
      let user = JSON.parse(storedUser);
      this.storedUser.id = user.id;
      this.storedUser.username = user.username;
      this.storedUser.pokemon = user.pokemon;
    }
  }

  //Add pokemon to the user's pokemon-list and update storage on catch pokemon
  onCatchPokemon(pokename: string){
    this.storedUser.pokemon.push(pokename);
    this.userService.updateUserPokemonList(this.storedUser.id, this.storedUser.pokemon)
    .subscribe(() => {localStorage.setItem("user", JSON.stringify(this.storedUser))})
  }

  //Navigate to trainer page
  onClickTrainer(){
    this.router.navigate(['trainer'])
  }

  //Logout
  onClickLogout(){
    this.userService.logout();
  }

}
