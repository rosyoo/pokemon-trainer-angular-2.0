import { Component, OnInit } from '@angular/core';
import { UserService } from 'src/app/services/user.service';
import { Router } from '@angular/router';
import { NgForm } from '@angular/forms';

@Component({
  selector: 'app-login',
  templateUrl: './login.page.html',
  styleUrls: ['./login.page.css']
})
export class LoginPage implements OnInit {
  storedUser: string | null = null; 

  constructor(private router: Router, private userService: UserService) { 
    this.storedUser = localStorage.getItem("user")
  }


  onLoginSubmit(form: NgForm): void {
    console.log(form.value.username);
    const { username } = form.value;
    
    //Check if there is a user with username
    this.userService.checkUser(username).subscribe({
      next: (users) => {
        if(users[0]){ //if there is a user, save user to storage and navigate to pokedex
          localStorage.setItem("user", JSON.stringify(users[0]));
          this.router.navigate(["pokedex"])
        }else{ //if no user, create user, save to storage and navigate to pokemon
          this.userService.createUser(username).subscribe({
            next: (user) => {
              localStorage.setItem("user", JSON.stringify(user))
              this.router.navigate(["pokedex"])
            }
          })
        }
      },
      error: (error) => {
        console.log(error.message)
      }
    })

  } 
  
  ngOnInit(): void {
  }

}
