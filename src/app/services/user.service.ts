import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { environment } from 'src/environments/environment';
import { map, Observable, tap } from 'rxjs';
import { User} from '../models/user.model';
import { Router } from '@angular/router';

const userURL = environment.userApi;
const apiKey = environment.apiKey;

@Injectable({
  providedIn: 'root'
})

export class UserService {
  constructor(private http: HttpClient, private router: Router) {
  }
  private createHeaders(): HttpHeaders {
    return new HttpHeaders({
      'Content-Type': 'application/json',
      'x-api-key': apiKey,
    });
  }

  //check if user exists
  public checkUser(username : string): Observable<User[]> {
    return this.http.get<User[]>(`${userURL}?username=${username}`);
  }

  //create a user
  public createUser(username: string): Observable<User> {
    const user = {
      username,
      pokemon: [],
    };
    const headers = this.createHeaders();
    return this.http.post<User>(userURL, user, {headers})
  }

  //update user's list of pokemons
  public updateUserPokemonList(userId: number, PokemonList: string[]): Observable<User>{
    const user = {
      pokemon: PokemonList
    }
    const headers = this.createHeaders();
    return this.http.patch<User>(`${userURL}/${userId}`, user, {headers})
  }

  //logout function
  public logout(){
    sessionStorage.clear();
    localStorage.clear();
    this.router.navigate([""]);
  }
}
