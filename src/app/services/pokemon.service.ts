import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs'
import { environment } from "src/environments/environment"
import { Pokemon, PokemonResponse } from '../models/pokemon.model';

@Injectable({
  providedIn: 'root'
})
export class PokemonService {

  private _pokemonURL = environment.pokemonApi

  constructor(private http: HttpClient) { }

    getAllPokemons(): Observable<PokemonResponse>{
    return this.http.get<PokemonResponse>(`${this._pokemonURL}`)
  }

}
