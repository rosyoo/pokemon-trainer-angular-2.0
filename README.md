# PokemonTrainerAngular

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 13.2.3.

## Information
### Login page
Create or login user in the Login Page.
If user exist in the api, user will be redirected to pokedex page
If the user does not exist, an user will be created with the username input to local storage,
and redirected to pokedex page

### Pokedex page
List of pokemons to catch, click on catch button to add pokemon to collection
click on Trainer button to navigate to Trainer Page

### Trainer Page
List of pokemons that have been caught, click on release button to delete pokemon from collection

## Setup:
Clone the repository. Requires [Node] and [npm] to be installed
```
npm install 
npm install -g @angular/cli
```
Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

## Build
Run `ng build` to build the project. The build artifacts will be stored in the `dist/` directory.

## Heroku
link for the app in Heroku
[https://tranquil-reaches-68674.herokuapp.com/](https://tranquil-reaches-68674.herokuapp.com/)

## Author
Rosy Oo